import React, { Component } from 'react';
import { Container, Text, Card, List, ListItem, Thumbnail, Form, Item, Input, Label, Icon, Content } from 'native-base';
import { Image, StyleSheet, View, } from 'react-native';
// import ImageContainer from './ImageContainer';
import logo from "../../assets/images/robot-dev.png";
import { Avatar } from 'react-native-elements';
import HeaderComponent from '../../components/Header';
import * as apiRoutes from "../../apiroutes";
import axios from 'axios';
import * as Animatable from "react-native-animatable";
export default class ProfileInfo extends Component {
    static navigationOptions = {
        header: null,
    };
    state = {
        profileData: {}
    }
    componentDidMount() {
        const endPointUrlProfileData = apiRoutes.host + apiRoutes.profileData
        console.log("endPointUrlProfileData : ", endPointUrlProfileData)
        axios
            .get(endPointUrlProfileData)
            .then(response => {
                console.log("#response.data active jobs : ", response.data)
                this.setState({
                    profileData: response.data
                })
            }).catch(error => console.log("error activejob: ", error)
            )
    }
    render() {
        let profile = this.state.profileData
        return (
            <View style={styles.container}>
                <HeaderComponent
                    title="Profile Info"
                    leftIcon="keyboard-arrow-left"
                    navigation={this.props.navigation}
                />
                <View style={styles.imageContainer}>
                    {/* Avatar with Icon */}
                    <Avatar rounded size="xlarge" icon={{ name: 'user', type: 'font-awesome' }} />
                </View>
                <Content padder>
                    <Form>
                        <Animatable.View
                            animation="fadeInDown"
                            // easing="ease-in-circ"
                            delay={2}
                        >
                            <Card style={{ borderRadius: 5, padding: 10, backgroundColor: "#e1e9eb" }}>
                                <Animatable.View
                                    animation="pulse"
                                    // easing="ease-in-circ"
                                    delay={2}
                                    iterationCount={3}
                                >
                                    <Text>Full Name: {profile.first_name} {profile.last_name}</Text>
                                </Animatable.View>
                                {/* <Item fixedLabel underline={false}>
                        </Item> */}
                            </Card>
                        </Animatable.View>
                        <Animatable.View
                            animation="fadeInDown"
                            // easing="ease-in-circ"
                            delay={2}
                        >
                            <Card style={{ borderRadius: 5, padding: 10, backgroundColor: "#e1e9eb" }}>
                                <Text>CNIC #: {profile.cnic_no}</Text>
                            </Card>
                        </Animatable.View>
                        <Animatable.View
                            animation="fadeInDown"
                            // easing="ease-in-circ"
                            delay={2}
                        >
                            <Card style={{ borderRadius: 5, padding: 10, paddingTop: 10, paddingBottom: 50, backgroundColor: "#e1e9eb" }}>
                                <Text>Address: {profile.address}</Text>
                                {/* <Textarea rowSpan={5} bordered placeholder="Textarea" /> */}
                            </Card>
                        </Animatable.View>
                        <Animatable.View
                            animation="fadeInDown"
                            // easing="ease-in-circ"
                            delay={2}
                        >
                            <Card style={{ borderRadius: 5, padding: 10, backgroundColor: "#e1e9eb", justifyContent: 'space-between', flexDirection: "row" }}>
                                <Text>Zip #: {profile.postal_code}</Text>
                                {/* <Item fixedLabel underline={false}>
                            </Item>
                            <Input /> */}
                                {/* <Icon name='information-circle' style={{ fontSize: 25 }} /> */}
                            </Card>
                        </Animatable.View>
                        <Animatable.View
                            animation="fadeInDown"
                            // easing="ease-in-circ"
                            delay={2}
                        >
                            <Card style={{ borderRadius: 5, padding: 10, backgroundColor: "#e1e9eb", justifyContent: 'space-between', flexDirection: "row" }}>
                                <Text>Phone #: {profile.phone_no}</Text>
                            </Card>
                        </Animatable.View>
                        <Animatable.View
                            animation="fadeInDown"
                            // easing="ease-in-circ"
                            delay={2}
                        >
                            <Card style={{ borderRadius: 5, padding: 10, backgroundColor: "#e1e9eb", justifyContent: 'space-between', flexDirection: "row" }}>
                                <Text>Email #: {profile.email}</Text>
                            </Card>
                        </Animatable.View>
                        <Animatable.View
                            animation="fadeInDown"
                            // easing="ease-in-circ"
                            delay={2}
                        >
                            <Card style={{ borderRadius: 5, padding: 10, backgroundColor: "#e1e9eb", justifyContent: 'space-between', flexDirection: "row" }}>
                                <Text>Phone No #: {profile.phone_no}</Text>
                            </Card>
                        </Animatable.View>
                        <Animatable.View
                            animation="fadeInUp"
                            // easing="ease-in-circ"
                            delay={3}
                        >
                            <Card style={{ borderRadius: 5, padding: 10, backgroundColor: "#e1e9eb", justifyContent: 'space-between', flexDirection: "row" }}>
                                <Text>Works Area #: {profile.work_area ? profile.work_area : ""}</Text>
                            </Card>
                        </Animatable.View>
                    </Form>
                </Content>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        textAlign: "center"
    },
    containerCard: {
        width: "90%",
        alignSelf: "center",
        height: "65%",
        borderRadius: 15,
        marginBottom: 15
    },
    title: {
        fontWeight: "bold",
        fontSize: 20,
        textAlign: "center"
    },
    imageContainer: {
        alignItems: 'center',
        marginTop: 20,
        marginBottom: 0,
    },
    welcomeImage: {
        width: 100,
        height: 100,
        borderRadius: 100,
        resizeMode: 'contain',
        marginTop: 60,
    },
});