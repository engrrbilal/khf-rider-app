import React, { Component } from "react";
import { ScrollView, StyleSheet, View, Text, ImageBackground, ActivityIndicator, PixelRatio } from "react-native";
import HeaderComponent from "../../components/Header";
import { Container, Header, Content, Input, Item, Left, Body, Card, CardItem, Right, Button, Icon, Title, List, ListItem, Label, DatePicker } from 'native-base';
import { TouchableOpacity } from "react-native-gesture-handler";
import axios from 'axios';
import * as Animatable from "react-native-animatable";
import * as apiRoutes from "../../apiroutes";
import Colors from "../../constants/Colors";
// import cardBg1 from '../../assets/images/cardBg1.png';
// import cardBg2 from '../../assets/images/cardBg2.png';
// import cardBg3 from '../../assets/images/cardBg3.png';
// import cardBg4 from '../../assets/images/cardBg4.png';
// import cardBg5 from '../../assets/images/cardBg5.png';
// import cardBg6 from '../../assets/images/cardBg6.png';

export default class AccountScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false,
      chosenDate: new Date(),
      chosenDate2: new Date()

    };
    this.setDate = this.setDate.bind(this)
    this.setDate2 = this.setDate2.bind(this)
  }


  setDate(newDate) {
    console.log(this.state)
    this.setState({
      chosenDate: newDate
    })

  }

  setDate2(newDate) {
    console.log(this.state)
    this.setState({
      chosenDate2: newDate
    })

  }
  componentDidMount() {
    const endPointUrlAccountData = apiRoutes.host + apiRoutes.accountDetail
    console.log("endPointUrlAccountData : ", endPointUrlAccountData)

    this.setState({
      isLoading: true
    }, () => {
      axios
        .get(endPointUrlAccountData)
        .then(response => {
          console.log("#response.data acounts: ", response.data)
          this.setState({
            accountData: response.data,
            isLoading: false
          })
        }).catch((error) => {
          console.log("error account: ", error)
          this.setState({
            isLoading: false
          })
        })
    })
  }
  render() {
    // let cardsBackgroundImages = {
    //   0: cardBg1,
    //   1: cardBg2,
    //   2: cardBg3,
    //   3: cardBg4,
    //   4: cardBg5,
    //   5: cardBg6,
    // }
    return (
      <View style={{ flex: 1 }}>
        <HeaderComponent
          title="Accounts"
          leftIcon="keyboard-arrow-left"
          navigation={this.props.navigation}
        />
        <ScrollView style={styles.container}>
          {/* <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginLeft: 10, marginRight: 10 }}>
            <View style={styles.borderBoxStyle}>
              <DatePicker
                // defaultDate={new Date(2020, 1, 1)}
                // minimumDate={new Date()}
                // maximumDate={new Date(2020, 12, 31)}
                locale={"en"}
                timeZoneOffsetInMinutes={undefined}
                modalTransparent={false}
                animationType={"fade"}
                androidMode={"default"}
                placeHolderText=" From"
                textStyle={{ color: "green" }}
                placeHolderTextStyle={{ color: "grey", fontSize: 20 }}
                onDateChange={this.setDate}
                disabled={false}
                style={{ color: 'red' }}
              />

              <Icon name='calendar' style={{ fontSize: 20 }} />
            </View>
            <View style={styles.borderBoxStyle}>
              <DatePicker
                // defaultDate={new Date(2020, 1, 1)}
                // minimumDate={new Date(2018, 0, 1)}
                // maximumDate={new Date(2020, 12, 31)}
                locale={"en"}
                timeZoneOffsetInMinutes={undefined}
                modalTransparent={false}
                animationType={"fade"}
                androidMode={"default"}
                placeHolderText=" To"
                textStyle={{ color: "green" }}
                placeHolderTextStyle={{ color: "grey", fontSize: 20 }}
                onDateChange={this.setDate2}
                disabled={false}

              />

              <Icon name='calendar' style={{ fontSize: 20}} />
            </View>
          </View> */}
          {/* <Button style={{ backgroundColor: '#03a9fc', width: '50%', justifyContent: 'center', alignSelf: 'center' }}><Text style={{ color: 'white', fontWeight: 'bold', fontSize: 17 }}> Search</Text></Button> */}
          <Content padder>
            {/* <Button full>
            <Text style={{color:"#ffff"}}>Search</Text>
          </Button> */}
            {this.state.accountData && this.state.accountData.length ? this.state.accountData.map((account, index) => {
              return (
                // { console.log("#account: ", account) }
                <View style={styles.cardsContainer} key={index}>
                  <TouchableOpacity onPress={() => this.props.navigation.navigate('AccountDetail', {
                    account: account,
                  })}>
                    <Animatable.View
                      animation={index === 0 ? "fadeInDown" : "fadeInLeft"}
                      // easing="ease-in-quad"
                      delay={1}
                    // iterationCount="infinite"
                    >
                      <Card style={{ borderRadius: 5 }}>
                        <CardItem header style={{ justifyContent: 'space-between' }}>
                          <Text style={{ fontWeight: 'bold', fontSize: 16 }}>{this.state.chosenDate.toDateString()}</Text>
                          <Text style={{ fontWeight: 'bold', fontSize: 16 }}>To</Text>
                          <Text style={{ fontWeight: 'bold', fontSize: 16 }}>{this.state.chosenDate2.toDateString()}</Text>
                        </CardItem>
                        <CardItem>
                          <Body style={{ flexDirection: 'row', justifyContent: 'space-between' }}>

                            <Text style={{ fontSize: 16 }} >
                              Payment Status :
                           </Text>
                            <Text style={{ fontSize: 16 }} >
                              {account["payment_status"]}
                            </Text>
                          </Body>
                        </CardItem>
                        <CardItem bordered style={{ backgroundColor: Colors.themeLightColor }}>
                          <Body style={{ flexDirection: 'row', justifyContent: 'space-between' }}>

                            <Text style={{ fontWeight: 'bold', color: "#ffff", fontSize: 20 }} >
                              Your Earning :
                           </Text>
                            <Animatable.View
                              animation="pulse"
                              // easing="ease-in-circ"
                              delay={2}
                              iterationCount={2}
                            >
                              <Text style={{ fontWeight: 'bold', color: "#ffff", fontSize: 20 }} >
                                {account["earning"]}
                              </Text>
                            </Animatable.View>
                          </Body>
                        </CardItem>

                      </Card>
                      {/* <ImageBackground
                        source={cardsBackgroundImages[index] ? cardsBackgroundImages[index] : cardsBackgroundImages[index % 6]}
                        style={{
                          width: "100%", height: "auto",
                          borderRadius: PixelRatio.getPixelSizeForLayoutSize(2)
                        }}
                        imageStyle={{
                          resizeMode: 'stretch',
                          borderRadius: PixelRatio.getPixelSizeForLayoutSize(2)
                        }}
                      >
                        <View header bordered style={{ justifyContent: 'space-between', flexDirection: "row" }}>
                          <Text style={{ fontWeight: 'bold', fontSize: 16 }}>{account["start_date"]}</Text>
                          <Text style={{ fontWeight: 'bold', fontSize: 16 }}>To</Text>
                          <Text style={{ fontWeight: 'bold', fontSize: 16 }}>{account["end_date"]}</Text>
                        </View>
                        <View bordered style={{ backgroundColor: Colors.themeLightColor }}>
                          <Body style={{ flexDirection: 'row', justifyContent: 'space-between' }}>

                            <Text style={{ fontWeight: 'bold', color: "#ffff", fontSize: 20 }} >
                              Total :
                             </Text>
                            <Text style={{ fontWeight: 'bold', color: "#ffff", fontSize: 20 }} >
                              5500
                         </Text>
                          </Body>
                        </View>
                      </ImageBackground> */}
                    </Animatable.View>

                  </TouchableOpacity>
                </View>

              )
            })
              :
              !this.state.isLoading ?
                <View style={styles.defaultTextContainer}>
                  <Text style={styles.defaultText}> No Account Data Avaible! </Text>
                </View> : <ActivityIndicator size="large" color={Colors.themeLightColor}/>
            }


            {/* <TouchableOpacity onPress={() => this.props.navigation.navigate('AccountDetail')} >
            <Card style={{ borderRadius: 5 }}>
              <CardItem header bordered style={{ justifyContent: 'space-between' }}>
                <Text style={{ fontWeight: 'bold', fontSize: 16 }}>{this.state.chosenDate.toDateString()}</Text>
                <Text style={{ fontWeight: 'bold', fontSize: 16 }}>To</Text>
                <Text style={{ fontWeight: 'bold', fontSize: 16 }}>{this.state.chosenDate2.toDateString()}</Text>
              </CardItem>
              <CardItem bordered style={{ backgroundColor: Colors.themeLightColor }}>
                <Body style={{ flexDirection: 'row', justifyContent: 'space-between' }}>

                  <Text style={{ fontWeight: 'bold', color: "#ffff", fontSize: 20 }} >
                    Total :
                </Text>
                  <Text style={{ fontWeight: 'bold', color: "#ffff", fontSize: 20 }} >
                    5500
                </Text>
                </Body>
              </CardItem>

            </Card>
          </TouchableOpacity> */}
            {/* <Title style={{ marginTop: 15 }}><Text style={{ color: 'black' }}>Current weak</Text></Title> */}
          </Content>
        </ScrollView>
      </View>
    );
  }
}

AccountScreen.navigationOptions = {
  header: null
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: "#fff"
  },
  borderBoxStyle: {
    flexDirection: "row",
    justifyContent: "space-between",
    borderRadius: 5,
    borderWidth: 1,
    borderColor: "#d6d7da",
    width: "45%",
    height: 40,
    justifyContent: "center", alignItems: "center",
    marginBottom: 15, marginTop: 15
  },
  defaultTextContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#FFF",
    color: "#cfefff"
  },
  defaultText: {
    fontSize: 18, fontWeight: "bold"
  }
});