import React from 'react'
import {
    StyleSheet, Text, View
} from 'react-native'
import { Body, Card, CardItem, List, ListItem, Icon, Content } from 'native-base';
import * as Animatable from "react-native-animatable";
import HeaderComponent from '../../components/Header';
import * as apiRoutes from "../../apiroutes";
import axios from 'axios';
import Colors from "../../constants/Colors";

export default class PaymentInfo extends React.Component {
    state = {
        paymentInfoData: {},
        isLoading: false
    }
    componentDidMount() {
        const endPointUrlAccountData = apiRoutes.host + apiRoutes.accountsBalanceDetail
        console.log("endPointUrlAccountData : ", endPointUrlAccountData)
        this.setState({
            isLoading: true
        }, () => {
            axios
                .get(endPointUrlAccountData)
                .then(response => {
                    console.log("#response.data acounts: ", response.data)
                    this.setState({
                        paymentInfoData: response.data,
                        isLoading: false
                    })
                }).catch((error) => {
                    console.log("error account: ", error)
                    this.setState({
                        isLoading: false
                    })
                })
        })
    }

    render() {
        const bankAccounts = this.state.paymentInfoData["bank_accounts"]
        const mobileAccounts = this.state.paymentInfoData["mobile_accounts"]
        console.log("bankAccounts : ", bankAccounts)
        // Object= {
        //     "bank_accounts": Array [
        //       Object {
        //         "account_no": "098490384903840",
        //         "account_title": "zahid hussain",
        //         "bank": "meezaan",
        //         "createdAt": "2020-02-24T20:57:33.000Z",
        //         "id": 1,
        //         "updatedAt": "2020-02-24T20:57:33.000Z",
        //         "user_id": 1,
        //       },
        //     ],
        //     "currentWeekBalance": 1200,
        //     "mobile_accounts": Array [
        //       Object {
        //         "cnic_no": "722729229222",
        //         "createdAt": "2020-02-24T20:59:27.000Z",
        //         "id": 1,
        //         "mobile_no": "030002323232",
        //         "service": "easy paisa",
        //         "updatedAt": "2020-02-24T20:59:27.000Z",
        //         "user_id": 1,
        //       },
        //     ],
        //     "oldBalance": 5800,
        //     "previousWeekBalance": 1000,
        //   }

        return (

            <View style={{ flex: 1 }}>
                <HeaderComponent
                    title="Payment Info"
                    leftIcon="keyboard-arrow-left"
                    navigation={this.props.navigation}
                />
                <Content padder>
                    <Animatable.View
                        animation="fadeInDown"
                        // easing="ease-in-circ"
                        delay={2}
                    // iterationCount={4}
                    >
                        <Card style={{ borderRadius: 5, padding: 10, backgroundColor: Colors.themeLightColor }}>
                            <View style={{ flexDirection: "row", justifyContent: "space-between", paddingTop: 5, paddingBottom: 5, paddingLeft: 15, paddingRight: 15 }}>
                                <Text style={{ color: "#ffff" }}>Previous Balance</Text>
                                <Text style={{ color: "#ffff" }}>Rs : {this.state.paymentInfoData && this.state.paymentInfoData.oldBalance}/=</Text>
                            </View>
                        </Card>
                    </Animatable.View>
                    <Animatable.View
                        animation="fadeInDown"
                        // easing="ease-in-circ"
                        delay={2}
                    // iterationCount={4}
                    >
                        <Card style={{ borderRadius: 5, padding: 10, backgroundColor: Colors.themeLightColor }}>
                            <View style={{ flexDirection: "row", justifyContent: "space-between", paddingTop: 5, paddingBottom: 5, paddingLeft: 15, paddingRight: 15 }}>
                                <Text style={{ color: "#ffff" }}>Last Week Balance</Text>
                                <Text style={{ color: "#ffff" }}>Rs : {this.state.paymentInfoData && this.state.paymentInfoData.previousWeekBalance}/=</Text>
                            </View>
                        </Card>
                    </Animatable.View>
                    <Animatable.View
                        animation="fadeInDown"
                        // easing="ease-in-circ"
                        delay={2}
                    // iterationCount={4}
                    >
                        <Card style={{ borderRadius: 5, padding: 10, backgroundColor: Colors.themeLightColor }}>
                            <View style={{ flexDirection: "row", justifyContent: "center", paddingTop: 5, paddingBottom: 5, paddingLeft: 15, paddingRight: 15 }}>
                                <Text style={{ color: "#ffff", fontSize: 22, fontWeight: "bold" }}>Bank Details / ID</Text>
                                {/* <Icon style={{color:"#ffff"}} name='information-circle' style={{ fontSize: 25}} /> */}
                            </View>
                            {bankAccounts && bankAccounts.map((account, index) => {
                                return (
                                    <View key={index}>
                                        <Animatable.View
                                            animation="fadeInDown"
                                        // easing="ease-in-circ"
                                        // delay={2}
                                        // iterationCount={4}
                                        >

                                            <View style={{ flexDirection: "row", justifyContent: "space-between", paddingTop: 5, paddingBottom: 5, paddingLeft: 15, paddingRight: 15 }}>
                                                <Text style={{ color: "#ffff" }}>Bank : </Text>
                                                <Text style={{ color: "#ffff" }}>{account.bank}</Text>
                                            </View>
                                            <View style={{ flexDirection: "row", justifyContent: "space-between", paddingTop: 5, paddingBottom: 5, paddingLeft: 15, paddingRight: 15 }}>
                                                <Text style={{ color: "#ffff" }}>Account Title : </Text>
                                                <Text style={{ color: "#ffff" }}>{account.account_title}</Text>
                                            </View>
                                            <View style={{ flexDirection: "row", justifyContent: "space-between", paddingTop: 5, paddingBottom: 5, paddingLeft: 15, paddingRight: 15 }}>
                                                <Text style={{ color: "#ffff" }}>Account No : </Text>
                                                <Text style={{ color: "#ffff" }}>{account.account_no}</Text>
                                            </View>
                                        </Animatable.View>
                                    </View>
                                )
                            })}
                        </Card>

                    </Animatable.View>
                    <Animatable.View
                        animation="fadeInDown"
                        // easing="ease-in-circ"
                        delay={2}
                    // iterationCount={4}
                    >

                        <Card style={{ borderRadius: 5, padding: 10, backgroundColor: Colors.themeLightColor }}>
                            <View style={{ flexDirection: "row", justifyContent: "center", paddingTop: 5, paddingBottom: 5, paddingLeft: 15, paddingRight: 15 }}>
                                <Text style={{ color: "#ffff", fontSize: 22, fontWeight: "bold" }}>Other Accounts Details</Text>
                                {/* <Icon style={{color:"#ffff"}} name='information-circle' style={{ fontSize: 25}} /> */}
                            </View>
                            {mobileAccounts && mobileAccounts.map((account, index) => {
                                return (
                                    <View key={index}>
                                        <Animatable.View
                                            animation="fadeInDown"
                                        // easing="ease-in-circ"
                                        // delay={2}
                                        // iterationCount={4}
                                        >
                                            <View style={{ flexDirection: "row", justifyContent: "space-between", paddingTop: 5, paddingBottom: 5, paddingLeft: 15, paddingRight: 15 }}>
                                                <Text style={{ color: "#ffff" }}>Service : </Text>
                                                <Text style={{ color: "#ffff" }}>{account.service}</Text>
                                            </View>
                                            <View style={{ flexDirection: "row", justifyContent: "space-between", paddingTop: 5, paddingBottom: 5, paddingLeft: 15, paddingRight: 15 }}>
                                                <Text style={{ color: "#ffff" }}>Account Registered No : </Text>
                                                <Text style={{ color: "#ffff" }}>{account.mobile_no}</Text>
                                            </View>
                                        </Animatable.View>
                                    </View>
                                )
                            })}
                        </Card>
                    </Animatable.View>
                </Content>
            </View>

        )

    }

}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});




