import * as WebBrowser from 'expo-web-browser';
import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Button,
} from 'react-native';
import { List, ListItem, Container, Content } from 'native-base';
import * as Animatable from "react-native-animatable";

import HeaderComponent from '../components/Header';
import * as apiRoutes from "../apiroutes"
import axios from 'axios';
import Colors from '../constants/Colors';

export default class JobScreen extends Component {

    componentDidMount() {
        console.log("componentDidMount JobScreen")
        this.willFocusSubscription = this.props.navigation.addListener(
            "willFocus",
            () => {
                console.log("componentDidMount willFocus JobScreen")
            }
        );
    }

    upadeStatusNotifierWithOrderId = () => {
        state = {
            isLoading: false
        }
        try {
            const { params } = this.props.navigation.state;
            const activeJob = params.activeJob
            const statusNotifierUrl = apiRoutes.host + apiRoutes.statusNotifierWithOrderId + activeJob.id
            console.log("#statusNotifierUrl : ", statusNotifierUrl)
            axios
                .put(statusNotifierUrl)
                .then((response) => {
                    console.log("#response.data : ", response.data)
                    this.setState(
                        {
                            isLoading: false
                        }, () => alert(response.data.msg))

                }).catch((error) => {
                    console.log("#error : ", error)
                })
        } catch (error) {
            console.log("#error : ", error)
            this.setState({
                errorStatus: true,
                errorMsg: error.message,
                isLoading: false
            });
        }
    }
    render() {
        // console.log("sss",this.props.navigation)
        const { params } = this.props.navigation.state;
        const activeJob = params.activeJob
        // console.log("#activeJob : ", activeJob)
        // console.log("#activeJob.status : ", activeJob.status)

        let pickupTime = new Date(activeJob.pickup_time)
        let pickupHours = pickupTime.getHours();
        let pickupMinutes = pickupTime.getMinutes();
        pickupTime = pickupHours + ':' + pickupMinutes;

        let deliveryTime = new Date(activeJob.delivery_time)
        let deliveryHours = deliveryTime.getHours();
        let deliveryMinutes = deliveryTime.getMinutes();
        deliveryTime = deliveryHours + ':' + deliveryMinutes;

        let today = new Date(activeJob.pickup_time);
        let dd = today.getDate();

        let mm = today.getMonth() + 1;
        let yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }

        if (mm < 10) {
            mm = '0' + mm;
        }
        today = mm + '-' + dd + '-' + yyyy;

        return (
            <View style={styles.container}>
                <Container>
                    <HeaderComponent
                        title={"Order Detail"}
                        leftIcon="arrow-back"
                        navigation={this.props.navigation}
                    />
                    <Content padder>
                        <Animatable.View
                            animation="fadeInDown"
                        // easing="ease-in-circ"
                        // delay={1}
                        // iterationCount={4}
                        >

                            <List>
                                <ListItem style={{ justifyContent: 'space-between' }}>
                                    <Text style={{ fontSize: 16 }}>Pickup Time :</Text>
                                    <Text style={{ fontSize: 16 }}>{pickupTime}</Text>
                                </ListItem>
                                <ListItem style={{ justifyContent: 'space-between' }}>
                                    <Text style={{ fontSize: 16 }}>Delivery Time :</Text>
                                    <Text style={{ fontSize: 16 }}>{deliveryTime}</Text>
                                </ListItem>
                                <ListItem style={{ justifyContent: 'space-between' }}>
                                    <Text style={{ fontSize: 16 }}>Chef Name :</Text>
                                    <Text style={{ fontSize: 16 }}>{activeJob.chef.first_name} {activeJob.chef.last_name}</Text>
                                </ListItem>
                                {activeJob.chef.phone_no ?
                                    <ListItem style={{ justifyContent: 'space-between' }}>
                                        <Text style={{ fontSize: 16 }}>Chef Phone No:</Text>
                                        <Text style={{ fontSize: 16 }}>{activeJob.chef.phone_no}</Text>
                                    </ListItem> : undefined
                                }
                                <ListItem style={{ justifyContent: 'space-between' }}>
                                    <Text style={{ fontSize: 16 }}>Customer Name :</Text>
                                    <Text style={{ fontSize: 16 }}>{activeJob.customer_name}</Text>
                                </ListItem>
                                <ListItem style={{ justifyContent: 'space-between' }}>
                                    <Text style={{ fontSize: 16 }}>Customer Phone No:</Text>
                                    <Text style={{ fontSize: 16 }}>{activeJob.phone_no}</Text>
                                </ListItem>
                                <ListItem style={{ justifyContent: 'space-between' }}>
                                    <Text style={{ fontSize: 16 }}>Date :</Text>
                                    <Text style={{ fontSize: 16 }}>{today}</Text>
                                </ListItem>
                                <ListItem style={{ justifyContent: 'space-between' }}>
                                    <Text style={{ fontSize: 16 }}>Your Earning :</Text>
                                    <Text style={{ fontSize: 16 }}>{activeJob.rider_earning}</Text>
                                </ListItem>
                            </List>
                        </Animatable.View>
                    </Content>
                    {activeJob.status === "Delivered" ?
                        <Animatable.View
                            animation="fadeInUp"
                            // easing="ease-in-circ"
                            delay={2}
                        // iterationCount={4}
                        >
                            <View style={{ flexDirection: "row", justifyContent: "space-around", paddingTop: 5, paddingBottom: 5, paddingLeft: 15, paddingRight: 15 }}>
                                <View style={{ width: 300 }}>
                                    <Button title="Order Delivered" disabled style={{ borderRadius: 10, backgroundColor: "green" }} />
                                </View>
                            </View>

                        </Animatable.View>
                        :
                        <Animatable.View
                            animation="fadeInUp"
                            // easing="ease-in-circ"
                            delay={2}
                        // iterationCount={4}
                        >
                            <View style={{ flexDirection: "row", justifyContent: "space-around", paddingTop: 5, paddingBottom: 5, paddingLeft: 15, paddingRight: 15 }}>
                                <View style={{ width: 300 }}>
                                    <Button title={activeJob.status} onPress={this.upadeStatusNotifierWithOrderId} style={{ borderRadius: 10, backgroundColor: Colors.themeDarkColor }} />
                                </View>
                            </View>
                        </Animatable.View>

                    }
                </Container>
                {/* <LocationAccess/> */}
            </View>
        );
    }
}

JobScreen.navigationOptions = {
    header: null,
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    tabBarInfoText: {
        // marginTop: 40,
        fontSize: 20,
        fontWeight: "bold",
        color: 'rgba(96,100,109, 1)',
        textAlign: 'center',
    },
});







// import * as WebBrowser from 'expo-web-browser';
// import React, { Component } from 'react';
// import {
//     Platform,
//     StyleSheet,
//     Text,
//     View,
//     Button,
// } from 'react-native';
// import { Container, Header, Content, Card, CardItem, Body, Left, Icon, Title, Item } from "native-base";
// import { TouchableOpacity } from 'react-native-gesture-handler';
// import HeaderComponent from '../components/Header';

// export default class JobScreen extends Component {
//     render() {
//         // console.log("sss",this.props.navigation)
//         const { params } = this.props.navigation.state;
//         const activeJob =params.activeJob
//         console.log("#activeJob : ",activeJob)
//         return (
//             <View style={styles.container}>
//                 <Container>
//                     <HeaderComponent
//                         title="Current Job"
//                         leftIcon="arrow-back"
//                         navigation={this.props.navigation}
//                     />
//                     <Text style={styles.tabBarInfoText}>
//                         Order ID : {activeJob.orderId}
//                         </Text>
//                     <Content padder>
//                         <Card>
//                             <View style={{ flexDirection: "row", justifyContent: "space-between", paddingTop: 5, paddingBottom: 5, paddingLeft: 15, paddingRight: 15 }}>
//                                 <Text>Time</Text>
//                                 <Text>{activeJob.time}</Text>
//                             </View>
//                             {/* <View style={{ flexDirection: "row", justifyContent: "space-between", paddingTop: 5, paddingBottom: 5, paddingLeft: 15, paddingRight: 15 }}>
//                                 <Text>chat Id</Text>
//                                 <Text> #talha 10A</Text>
//                             </View> */}
//                             <View style={{ flexDirection: "row", justifyContent: "space-between", paddingTop: 5, paddingBottom: 5, paddingLeft: 15, paddingRight: 15 }}>
//                                 <Text>Location</Text>
//                                 <Text>{activeJob.location}</Text>
//                             </View>
//                         </Card>
//                     </Content>
//                     <TouchableOpacity onPress={() => this.props.navigation.navigate('GoogleMap', {
//                         orderId: 1511,
//                     })}>
//                         <View style={{ height: 200, marginTop: 20, alignSelf: "center", borderWidh: 1, borderColor: "black" }}>
//                             <Text>Map</Text>
//                         </View>

//                     </TouchableOpacity>
//                     <View style={{ flexDirection: "row", justifyContent: "space-around", paddingTop: 5, paddingBottom: 5, paddingLeft: 15, paddingRight: 15 }}>
//                         <Button title="Arrived" onPress={() => alert("Arrived")} />
//                         <Button title="pickedUp" onPress={() => alert("pickedUp")} />
//                     </View>
//                     <View style={{ flexDirection: "row", justifyContent: "center", paddingTop: 5, paddingBottom: 5, paddingLeft: 15, paddingRight: 15 }}>
//                         <Button title="Report A Problem" onPress={() => alert("Report A Problem")} />
//                     </View>
//                 </Container>
//                 {/* <LocationAccess/> */}
//             </View>
//         );
//     }
// }

// JobScreen.navigationOptions = {
//     header: null,
// };

// const styles = StyleSheet.create({
//     container: {
//         flex: 1,
//         backgroundColor: '#fff',
//     },
//     tabBarInfoText: {
//         // marginTop: 40,
//         fontSize: 20,
//         fontWeight: "bold",
//         color: 'rgba(96,100,109, 1)',
//         textAlign: 'center',
//     },
// });
