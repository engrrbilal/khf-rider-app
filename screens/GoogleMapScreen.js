import * as WebBrowser from 'expo-web-browser';
import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
} from 'react-native';
// import { Container, Header, Content, Card, CardItem, Body, Left, Icon, Title, Item, Button } from "native-base";
import LocationAccess from '../components/LocationAccess';
import HeaderComponent from '../components/Header';

export default class GoogleMapScreen extends Component {
    render() {
        return (
            <View style={styles.container}>
                <HeaderComponent
                    title="Current Job Map"
                    leftIcon="arrow-back"
                    navigation={this.props.navigation}
                />
                <Text style={styles.tabBarInfoText}>Map Screen</Text>
                <LocationAccess />
            </View>
        );

    }
}

GoogleMapScreen.navigationOptions = {
    header: null,
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },

    tabBarInfoText: {
        fontSize: 20,
        // marginTop: 40,
        color: 'rgba(96,100,109, 1)',
        textAlign: 'center',
    },
});
