
export const host = "http://ec2-3-0-57-66.ap-southeast-1.compute.amazonaws.com:3000";
export const login = "/auth/rider/login";
export const activeJobs = "/rider/order/active";
export const profileData = "/rider/profile";
export const completedJobs = "/rider/order/completed";
export const statusNotifierWithOrderId = "/rider/order/";

export const accountDetail = "/rider/account/details";
export const accountsBalanceDetail = "/rider/account/balance/details";