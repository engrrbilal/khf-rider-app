const tintColor = "#2f95dc";

export default {
  headerBackgroundColor:"#cd1a74",
  themeDarkColor:"#cd1a74",
  themeLightColor:"#ec6ba8",
  // themeColor3:"blue",
  cardColor1:"#cfefff",
  cardColor2:"#329cd1",

  tintColor,
  tabIconDefault: "#ccc",
  tabIconSelected: tintColor,
  tabBar: "#fefefe",
  errorBackground: "red",
  errorText: "#fff",
  warningBackground: "#EAEB5E",
  warningText: "#666804",
  noticeBackground: tintColor,
  noticeText: "#fff",

  titleColor: "#317eac",
  titleBackground: "#f5f5f5",
  success: "#DEF0D8",
  danger: "#F2DEDF",
  modalHeaderFooterBg: "#666666",
  modalContentBg: "#ffffff",
  iconColorBlue: "#2fa4e7",

  whiteColor: "white",
  borderColor: "#dddddd",
  textBlueColor: "#2fa4e7"
};
