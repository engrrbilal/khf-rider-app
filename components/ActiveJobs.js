import * as WebBrowser from 'expo-web-browser';
import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    ScrollView,
    ActivityIndicator,
    ImageBackground,
    PixelRatio,
    AsyncStorage
} from 'react-native';
import { Container, Header, Content, Card, CardItem, Body } from "native-base";
import { TouchableOpacity } from 'react-native-gesture-handler';
import * as Animatable from "react-native-animatable";
import axios from 'axios';
import Colors from '../constants/Colors';
import cardBg1 from '../assets/images/cardBg1.png';
import cardBg2 from '../assets/images/cardBg2.png';
import cardBg3 from '../assets/images/cardBg3.png';
import cardBg4 from '../assets/images/cardBg4.png';
import cardBg5 from '../assets/images/cardBg5.png';
import cardBg6 from '../assets/images/cardBg6.png';

export default class ActiveJobs extends Component {
    state = {
        jobsData: [],
        isLoading: false
    }
    componentDidMount() {
        console.log("componentDidMount ActiveJobs")
        this.willFocusSubscription = this.props.navigation.addListener(
            "willFocus",
            () => {
                console.log("componentDidMount willFocus activejob")
                this.fetchData();
            }
        );
    }
    fetchData() {
        this.setState({
            isLoading: true
        }, () => {
            axios
                .get(this.props.endPointUrl)
                .then(response => {
                    console.log("#response.data active jobs : ", response.data)
                    this.setState({
                        jobsData: response.data,
                        isLoading: false
                    })
                }).catch((error) => {
                    console.log("error activejob: ", error)
                    this.setState({
                        isLoading: false
                    })
                })
        })
    }
    componentDidUpdate(nextProps, prevState) {
        console.log("componentDidUpdate ActiveJobs")
    }
    componentWillUnmount() {
        console.log("componentWillUnmount ActiveJobs")
    }
    render() {
        let today = ''
        let cardsBackgroundImages = {
            0: cardBg1,
            1: cardBg2,
            2: cardBg3,
            3: cardBg4,
            4: cardBg5,
            5: cardBg6,
        }
        return (
            <View style={styles.container}>
                {/* <Text style={styles.tabBarInfoText}>
                    {this.props.recipee}
                </Text> */}
                <Container>
                    <ScrollView>
                        {/* <View style={{ justifyContent: "center", alignItems: "center" }}>
                            {this.state.isLoading ? <ActivityIndicator size="large" /> : undefined}
                        </View> */}
                        {this.state.jobsData.length ? this.state.jobsData.map((activeJob, index) => {
                            pickupTime = new Date(activeJob.pickup_time)
                            deliveryTime = new Date(activeJob.delivery_time)
                            let pickupHours = pickupTime.getHours();
                            let pickupMinutes = pickupTime.getMinutes();
                            pickupTime = pickupHours + ':' + pickupMinutes;

                            let deliveryHours = deliveryTime.getHours();
                            let deliveryMinutes = deliveryTime.getMinutes();
                            deliveryTime = deliveryHours + ':' + deliveryMinutes;
                            return (
                                // { console.log("#activeJob: ", activeJob) }
                                <View style={styles.cardsContainer} key={index}>
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('JobScreen', {
                                        activeJob: activeJob,
                                    })}>
                                        <Animatable.View
                                            animation={index === 0 ? "fadeInDown" : "fadeInLeft"}
                                            // easing="ease-in-quad"
                                            delay={1}
                                        // iterationCount="infinite"
                                        >
                                            <View>
                                                <ImageBackground
                                                    source={cardsBackgroundImages[index] ? cardsBackgroundImages[index] : cardsBackgroundImages[index % 6]}
                                                    style={{
                                                        width: "100%", height: "auto",
                                                        borderRadius: PixelRatio.getPixelSizeForLayoutSize(2)
                                                    }}
                                                    imageStyle={{
                                                        resizeMode: 'stretch',
                                                        borderRadius: PixelRatio.getPixelSizeForLayoutSize(2)
                                                    }}
                                                >
                                                    <View style={{ paddingTop: 3, paddingBottom: 3, paddingLeft: 6, paddingRight: 6 }} key={index}>
                                                        <TouchableOpacity onPress={() => this.props.navigation.navigate('JobScreen', {
                                                            activeJob: activeJob,
                                                        })}>
                                                            <View>
                                                                <View style={[styles.cardsTopPart]}>
                                                                    <Text style={{ fontSize: 20 }}>Chef Name</Text>
                                                                    <Text style={{ fontSize: 20,fontWeight:"bold"}}>{activeJob.chef.first_name} {activeJob.chef.last_name}</Text>
                                                                </View>
                                                                <View style={[styles.cardsTopPart]}>
                                                                    <Text style={{ fontSize: 20 }}>Delivery Time</Text>
                                                                    <Text style={{ fontSize: 20,fontWeight:"bold"}}> {deliveryTime}</Text>
                                                                </View>
                                                            </View>
                                                        </TouchableOpacity>
                                                    </View>
                                                </ImageBackground>
                                                <View style={[styles.cardsBottomPartContainer, { backgroundColor: Colors.themeLightColor }]}>
                                                    <Text style={styles.cardsBottomPartText}>Pickup Time</Text>
                                                    <Text style={styles.cardsBottomPartText}> {pickupTime} </Text>
                                                </View>
                                            </View>
                                        </Animatable.View>

                                    </TouchableOpacity>
                                </View>

                            )
                        })
                            :
                            !this.state.isLoading ?
                                <View style={styles.defaultTextContainer}>
                                    <Text style={styles.defaultText}> No {this.props.title ? this.props.title : "Active Order"}! </Text>
                                </View> : <ActivityIndicator size="large" color={Colors.themeLightColor}/>
                        }
                    </ScrollView>
                </Container>
                {/* <quantityAccess/> */}
            </View>
        );
    }
}

ActiveJobs.navigationOptions = {
    header: null,
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    cardsContainer: { paddingTop: 2, paddingBottom: 2, paddingLeft: 6, paddingRight: 6 },
    cardsTopPart: {
        flexDirection: "row", fontSize: 18, justifyContent: "space-between",
        padding: 15
    },
    cardsBottomPartContainer: {
        flexDirection: "row", fontSize: 20, fontWeight: "bold",
        justifyContent: "space-between", padding:10,paddingLeft:17,paddingRight:17,
        margin:0,
        borderBottomLeftRadius:PixelRatio.getPixelSizeForLayoutSize(3),
        borderBottomRightRadius:PixelRatio.getPixelSizeForLayoutSize(3)
    },
    cardsBottomPartText: { color: "white", fontWeight: "bold", fontSize: 20 },
    defaultTextContainer: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#FFF",
        color: "#cfefff"
    },
    defaultText: {
        fontSize: 18, fontWeight: "bold"
    }
});