import React from "react";
import { View, ActivityIndicator } from "react-native";
import Colors from "../constants/Colors";

export default function Spinner(props) {
  return (
    <View
      style={{
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
      }}
    >
      <ActivityIndicator size={props.size || "large"} color={Colors.themeLightColor}/>
    </View>
  );
}
